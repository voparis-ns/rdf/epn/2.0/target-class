@base <http://voparis-ns.obspm.fr/rdf/epn/2.0/target-class>.
@prefix : <#>.

@prefix dc: <http://purl.org/dc/terms/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/>.
@prefix ivoasem: <http://www.ivoa.net/rdf/ivoasem#>.
@prefix skos: <http://www.w3.org/2004/02/skos/core#>.
@prefix daci: <http://purl.org/datacite/v4.4/>.

<> a owl:Ontology;
    dc:created "2023-11-11";
    dc:creator [ foaf:name "Cecconi, B." ],
    [ foaf:name "Erard, S." ];
    dc:license <http://creativecommons.org/publicdomain/zero/1.0/>;
    rdfs:label "Product Types for EPNcore metadata"@en;
    dc:title "Product Types for EPNcore metadata"@en;
    dc:description """The target_class parameter identifies the type of the target. Solar System
bodies are defined without ambiguity by the couple target_class and target_name;
in other cases, targets may have no proper name (i.e., samples). This parameter must
be informed whenever a value for target_name is provided.""";
    ivoasem:vocflavour "RDF Class".

dc:created a owl:AnnotationProperty.
dc:creator a owl:AnnotationProperty.
dc:title a owl:AnnotationProperty.
dc:description a owl:AnnotationProperty.
<#asteroid> a rdfs:Class;
  rdfs:label "Asteroid";
  rdfs:comment "Asteriod";
  skos:related <http://voparis-ns.obspm.fr/epn/2.0/target_class#dwarf_planet>;
  skos:related <http://voparis-ns.obspm.fr/epn/2.0/target_class#comet>.

<#calibration> a rdfs:Class;
  rdfs:label "Calibration";
  rdfs:comment """Calibration is used for observations only related 
            to instrument or signal calibration, including dark current, flat field, 
            reference sample (in lab), etc. Use of "calibration" with planetary bodies 
            is left to data providers.""".

<#comet> a rdfs:Class;
  rdfs:label "Comet";
  rdfs:comment "Comet";
  skos:related <http://voparis-ns.obspm.fr/epn/2.0/target_class#asteriod>.

<#dwarf_planet> a rdfs:Class;
  rdfs:label "Dwarf Planet";
  rdfs:comment "Dwarf Planet";
  skos:related <http://voparis-ns.obspm.fr/epn/2.0/target_class#planet>;
  skos:related <http://voparis-ns.obspm.fr/epn/2.0/target_class#asteriod>.

<#exoplanet> a rdfs:Class;
  rdfs:label "Exoplanet";
  rdfs:comment "Extra-solar Planet";
  skos:exactMatch <http://www.ivoa.net/rdf/object-type#planet>.

<#interplanetary_medium> a rdfs:Class;
  rdfs:label "Interplanetary Medium";
  rdfs:comment """Interplanetary Medium refers in particular to interplanetary 
            dust observed in context (not to samples).""";
  skos:related <http://voparis-ns.obspm.fr/epn/2.0/target_class#sky>.

<#planet> a rdfs:Class;
  rdfs:label "Planet";
  rdfs:comment "Planet".

<#sample> a rdfs:Class;
  rdfs:label "Sample";
  rdfs:comment """Sample refers to lunar or planetary samples, to meteorites, 
            but also to terrestrial samples, e.g., in laboratory studies.""".

<#satellite> a rdfs:Class;
  rdfs:label "Satellite";
  rdfs:comment """Satellite stands for natural satellites only -
            artificial satellites are handled though spacecraft or 
            spacejunk.""";
  skos:related <http://voparis-ns.obspm.fr/epn/2.0/target_class#spacecraft>.

<#sky> a rdfs:Class;
  rdfs:label "Sky";
  rdfs:comment """Sky should be used for all other celestial bodies, 
            usually referred to by their sky coordinates. It also includes the 
            Interstellar Medium.""".

<#spacecraft> a rdfs:Class;
  rdfs:label "Spacecraft";
  rdfs:comment "Spacecraft".

<#spacejunk> a rdfs:Class;
  rdfs:label "Spacejunk";
  rdfs:comment "Spacejunk".

<#star> a rdfs:Class;
  rdfs:label "Star";
  rdfs:comment "Star";
  skos:exactMatch <http://www.ivoa.net/rdf/object-type#star>.

